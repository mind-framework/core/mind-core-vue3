//Importar helpers
import fetch from './helper/fetch'
import Model from './helper/model'

export {
	fetch,
	Model
}

// Importar Element Plus
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import * as ElementPlusIconsVue from '@element-plus/icons-vue'

//Importar componentes
import mindApp from './components/mind-app'
import mindView from './components/mind-view'
import mindTable from './components/mind-table'
import mindForm from './components/mind-form'
import mindFilter from './components/mind-filter'
import mindDashboard from './components/mind-dashboard'
import mindWidgetContainer from './components/mind-widget-container'
import mindWidgetIndicatorText from './components/widgets/indicator-text'
import mindWidgetChart from './components/widgets/chart'
import mindReport from './components/mind-report'
// import chartJs from './components/widgets/chartjs'



export default (app) => {

	// Registro Element Plus
	app.use(ElementPlus)
	for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
		app.component(key, component)
	}


	// Registro los componentes
	app.component('mind-app', mindApp)
	app.component('mind-view', mindView)
	app.component('mind-table', mindTable)
	app.component('mind-form', mindForm)
	app.component('mind-filter', mindFilter)
	app.component('mind-dashboard', mindDashboard)
	app.component('mind-widget-container', mindWidgetContainer)
	app.component('mind-widget-indicator-text', mindWidgetIndicatorText)
	app.component('mind-widget-chart', mindWidgetChart)
	app.component('mind-report', mindReport)
	// app.component('chartJs', chartJs)
}


