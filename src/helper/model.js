
// import Vue from 'vue';
import {reactive, ref} from 'vue'
import fetch  from './fetch'

/**
	 * @param {string} uri uri donde se realiza la consulta al servidor
	 * @param {string} key	nombre de la propiedad que es la clave primaria del recurso
	 * @param {Object} template	objeto que contiene los valores por defecto para nuevos reursos
	 * @param {int} cacheTimeout	tiempo, en segundos, durante el cual no se realizarán nuevas consultas al recurso. Un valor = 0 deshabilita el cache
 */
export default class Model {
	/**
	 * 
	 * @param {string} uri uri donde se realiza la consulta al servidor
	 * @param {string} key	nombre de la propiedad que es la clave primaria del recurso
	 * @param {Object} template	objeto que contiene los valores por defecto para nuevos reursos
	 * @param {int} cacheTimeout	tiempo, en segundos, durante el cual no se realizarán nuevas consultas al recurso. Un valor = 0 deshabilita el cache
	 */
	constructor({uri = '', key=null, template={}, cacheTimeout = 1} = {}) {
		this.cacheTimeout = cacheTimeout
		this.uri = uri
		this.key = key
		this.template = template
		this.state = reactive({ 
			isLoading: false,
			contexts: [],
		});
		const state = ref([])
		this.state.contexts.push({
			context: 'default',
			state,
			isLoading: false,
		})
	}
	isLoading(){
		return this.state.isLoading
	}
	getKey() {
		return this.key
	}

	/**
	 * 
	 * @param {String} [context=default] contexto
	 * @param {Number} cacheTimeout duracion de cache de datos, en segundos
	 * @param {Boolean} staleWhileValidate 	devuelve inmediatamente el resultado almacenado en el store, luego realiza el fetch
	 * 																			Nota: si se setea en true, el await o promesa finalizará inmediatamente
	 */
	async fetch({context = 'default', cacheTimeout = this.cacheTimeout, filters = [], limit = 1000, offset = 0, sort = [], staleWhileValidate= false} = {}) {
		return new Promise((resolve, reject)=> {
			if(!this.uri) {
				return reject('uri no especificado')
			}

			const ts = Date.now()

			let contextData = this.getContext(context)

			if(staleWhileValidate) {
				resolve(contextData.state)
			}


			if(contextData.lastFetched && (ts - contextData.lastFetched) < cacheTimeout * 1000) {
				resolve(contextData.state)
				return
			}

			if(contextData.isLoading) {
				resolve(contextData.state)
				return
			}

			contextData.isLoading = true
			this.state.isLoading = true

			let params = []
			if (filters.length > 0) {
				params.push(...filters)
			}

			if (sort.length > 0) {
				let s = 'sort=' + sort.join('&sort=')
				params.push(s)
			}
			if (limit) {
				params.push(`limit=${limit}`)
			}
			if (offset) {
				params.push(`offset=${offset}`)
			}


			return fetch(`${this.uri}?${params.join('&')}`)
				.then((response)=>{
					contextData.lastFetched = Date.now()
					// contextData.state.value = response && response.body ? response.body.data : []
					contextData.state.value = response && response.body ? response.body.data : []

					resolve(contextData.state)
				})
				.finally(()=>{
					this.state.isLoading = false
					contextData.isLoading = false
				}) 
		})
	}

	/**
	 * Envia solicitud de eliminacion de un recurso al servidor
	 * @param {Object|Number|String} item elemento a eliminar, puede ser un objeto (del cual se extrae el id) o el id del elemento a eliminar
	 * @param {String} context contexto del cual se quiere eliminar el item
	 */
	async delete(item, context='default') {
		let id = item
		const key = this.getKey()
		if(typeof item == "object") {
			id = item[key]
		} 
		if(!id) {
			return Promise.reject("ID inválido")
		}

		return fetch(`${this.uri}/${id}`, { method: 'DELETE', credentials: "same-origin", })
			.then(()=>{
				const contextData = this.getContext(context)
				const idx = contextData.state.value.findIndex(el => el[key] == id);
				if ( idx >= 0 ) {
					contextData.state.value.splice( idx, 1)
				}
			})

	}

	/**
	 * Devuelve una nueva instancia del modelo, con los valores definidos en el template
	 * Este método debe utilizarse cuando se desea crear un nuevo recurso en el servidor
	 * Nota: Para enviar los datos
	 */
	new() {
		return {...JSON.parse(JSON.stringify(this.template)), _isNew: true }
	}

	/**
	 * devuelve una copia del elemento especificado
	 * Primero busca el elemento en la lista de recursos, si no la encuentra, realiza un fetch al servidor
	 * @param {any} id key del elemento a buscar
	 * @param {string} context contexto de datos
	 */
	get(id, context='default') {
		return new Promise((resolve, reject)=>{
			this.state.isLoading = true
			const key = this.getKey()
			const contextData = this.getContext(context)
			const resource = contextData.state.value && contextData.state.value.find(el => el[key] == id);
			const ts = Date.now()
			if(resource && contextData.lastFetched && (ts - contextData.lastFetched) < this.cacheTimeout * 1000) {
				resolve(JSON.parse(JSON.stringify(resource)))
				this.state.isLoading = false
			} else {
				return fetch(`${this.uri}/${id}`)
					.then((response)=>{
						resolve(response.body.data)
					})
					.catch((e)=>{
						reject("no se pudo obtener el recurso:" + e.message)
					})
					.finally(()=>{
						this.state.isLoading = false
					}) 
			}

		})
	}

	/**
	 * @alias patch
	 * @param {Object} item 
	 */
	async update(item) {
		return this.patch(item)
	}

	/**
	 * Actualiza un elemento en el servidor
	 * @param {Object} item elemento a enviar al servidor
	 * @param {string='default'} context="default"
	 */
	async patch(item, context='default') {
		let uri, key, id
		key = this.getKey()
		id = item[key]
		uri = `${this.uri}/${id}`

		return fetch(uri, { method: 'PATCH', credentials: "same-origin", body: JSON.stringify(item) })
		.then((response)=>{
			response = response.body.data
			const contextData = this.getContext(context)
			
			const resource = contextData.state.value.find(el => el[key] == id);
			
			if (resource && contextData.state.value.indexOf(resource) >= 0) {
				contextData.state.value.splice(contextData.state.value.indexOf(resource), 1, response)
			} else {
				contextData.state.value.push(response)
			}
			return response
		})
	}


	/**
	 * @alias post
	 * @param {Object,Array} items elemento o lista de elementos a crear
	 */
	async create(items) {
		return this.post(items)
	}

	/**
	 * Envía solicitud de creación de recurso en el servidor
	 * @param {Object,Array} items elemento o lista de elementos a crear
	 */
	async post(items, context='default') {
		let uri
		if(!Array.isArray(items)) {
			items = [items]
		}
		uri = `${this.uri}`
		const contextData = this.getContext(context)

		return fetch(uri, { method: 'POST', credentials: "same-origin", body: JSON.stringify(items) })
		.then((response)=>{
			response = response.body.data
			contextData.state.value.push(...response)
			return response
		})
	}



	/**
	 * Envía un registro al servidor
	 * 
	 * @param {Object,Array} item elemento (para patch) o lista de elementos (para post) a enviar
	 */
	async submit(item, context='default') {
		const contextData = this.getContext(context)
		let method, uri, key, id
		if(item._isNew) {
			if(!Array.isArray(item)) {
				item = [item]
			}
			uri = `${this.uri}`
			method = 'POST'
		} else {
			method = 'PATCH'
			key = this.getKey()
			id = item[key]
			uri = `${this.uri}/${id}`
		}

		return fetch(uri, { method: method, credentials: "same-origin", body: JSON.stringify(item) })
		.then((response)=>{
			response = response.body.data
			if(method == 'POST') {
				contextData.state.value.push(...response)
			} else {
				const resource = contextData.state.value.find(el => el[key] == id);
				if (contextData.state.value.indexOf(resource) >= 0) {
					contextData.state.value.splice(contextData.state.value.indexOf(resource), 1, response)
				} else {
					contextData.state.value.push(...response)
				}
			}
			return response
		})
	}


	/**
	 * 
	 * @param {string} [context='default'] devuelve la lista de recursos del contexto especificado (por defecto contexto= 'default')
	 */
	getData(context = 'default') {
		const res = this.getContext(context)
		return  res ? res.state.value : []
	}

	/**
	 * 
	 * @param {string} [context='default'] devuelve el contexto especificado
	 */
	getContext(context = 'default') {

    let contextData =  this.state.contexts.find((c) => c.context == context)
    if(!contextData) {
      const state = ref([])
      contextData = reactive({
        lastFetched: null,
        context: context,
        isLoading: false,
        state:state
      })

      this.state.contexts.push(contextData)
    }
		return contextData
	}

	/**
	 * 
	 * @param {string} [context='default'] devuelve el state del contexto especificado
	 */
	getState(context = 'default') {
		const ctx = this.getContext(context)
		return ctx ? ctx.state : null
	}

	

	/**
	 * vacia la lista de datos del contexto especificado
	 * @param {string} context='default' contexto a resetear 
	 */
	reset(context='default') {
		const res = this.getContext(context)
		res.state.value = []
		res.value = []
	}
}
