// import Vue from 'vue';

// export let pepe = Vue.observable({ 
// });

export let Options = {
	parseJson: true,
	onError: null,
	on401: null,
	responseParser: null
}

function hacerNada(){
	
}

/**
 * Wrapper de la funcion nativa fetch
 * @param  {...any} args argumentos compatibles con fetch
 */
export async function fetch(...args)  {
	return window.fetch(...args)
		.then(async (response)=>{
			const status = response.status
			let body = await response.text()
			let res = {
				status: status,
				headers: response.headers,
				body: body
			}

			try {
				const p = JSON.parse(body)
				res.body = p
			} catch(e) {
				hacerNada(e)
			}
			switch (status) {
				case 200:
				case 201: 
					return res
				case 204:
					return "" 
				case 401:
					if(typeof Options.on401 == 'function') {
						Options.on401(res.body)
					} else {
						throw(res.body)
					}
					break;
				default:
					throw(res.body)
			}
		})
		.then((response)=>{
			if(!response)
				return response
			return typeof Options.responseParser == 'function' ? Options.responseParser(response.body) : response
		})
		.catch((error)=>{
			if(typeof Options.onError == 'function') {
				Options.onError(error)
			} 
			return Promise.reject(error)
		})
}

export default fetch

/**
 * @param {string} uri	uri de destino
 * @param {Object} params
 */
export function get(uri, {filters = [], limit, offset, sort = [] } = {}) {
	let params = []
	if (limit) {
		params.push(`limit=${limit}`)
	}
	if (offset) {
		params.push(`offset=${offset}`)
	}
	params.push(...filters)
	if (sort.length > 0) {
		let s = 'sort=' + sort.join('&sort=')
		params.push(s)
	}
	return fetch(`${uri}?${params.join('&')}`, { credentials: "same-origin" })
}	

