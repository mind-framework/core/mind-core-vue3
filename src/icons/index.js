import iconPdf from './pdf'
import iconXls from './xls'

export {
	iconPdf, 
	iconXls
}

